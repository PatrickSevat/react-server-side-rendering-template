/**
 * Created by Patrick on 19/07/2016.
 */
const initState = {showModal: false,
    form: 'logIn',
    loggedIn: false,
    signedUp: false};

const appReducer = (state = initState, action) => {
    switch (action.type) {
        case 'LOGGED-IN':
            return Object.assign({}, state, {
                loggedIn: true
            });
        case 'SIGNED-UP':
            return Object.assign({}, state, {
                signedUp: true
            });
        case 'SHOW-MODAL':
            return Object.assign({}, state, {
                showModal: true
            });
        case 'CLOSE-MODAL':
            return Object.assign({}, state, {
                showModal: false
            });
        case 'LOGGED-OUT':
            return Object.assign({}, state, {
                loggedIn: false,
                form: 'logIn'
            });
        case 'SWITCH-FORM':
            return Object.assign({}, state, {
                form: action.form
            });
        case 'SHOW-ERROR':
            return Object.assign({}, state, {
                error: action.message
            });
        default:
            return state
    }
};

exports.appReducer = appReducer;