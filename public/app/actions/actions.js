/**
 * Created by Patrick on 19/07/2016.
 */
export const logIn = () => {
    return {
        type: 'LOGGED-IN'
    }
};

export const signUp = () => {
    return {
        type: 'SIGNED-UP'
    }
};

export const showModal = () => {
    return {
        type: 'SHOW-MODAL'
    }
};

export const closeModal = () => {
    return {
        type: 'CLOSE-MODAL'
    }
};

export const logOut = () => {
    return {
        type: 'LOGGED-OUT'
    }
};

export const switchForm = (form) => {
    return {
        type: 'SWITCH-FORM',
        form
    }
};

export const showError = (err) => {
    return {
        type: 'SHOW-ERROR',
        message: err
    }
};

