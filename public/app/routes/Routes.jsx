import React from 'react';
import { Router, Route, Redirect, IndexRoute, IndexRedirect, hashHistory } from 'react-router';


//Components here
import Outer from '../components/Outer.jsx';
import Home from '../components/Home.jsx';
import LogIn from '../components/LogIn.jsx';



//Normally you would wrap <Router/> in a <Provider store={reduxStore} /> wrapper
//However, that doesnt really work wel with React-Engine as the Outer component also contains <html><header>, ect.
//So the Provider wrapping was done in the Outer component, go check it out

var Routes = (
        <Router history={hashHistory}>
            <Redirect from="/gohome" to="/home" />
            <Route path="/" component={Outer}>
                <IndexRedirect to="/home"/>
                <Route path="home" component={Home} />
                <Route path="login" component={LogIn} />
            </Route>
        </Router>

);

module.exports = Routes;