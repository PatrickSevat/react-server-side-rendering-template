
import mongoose from 'mongoose';
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');
const saltFactor = 10;
const maxLogins = 10;
const lockOutTime = 2 * 60 * 60 * 1000; //= 2 hours


const UserSchema = new Schema({
    username: {type: String, required: true, index: {unique: true }},
    password: {type: String, required: true},
    email: {type: String, required: true},
    loginAttempts: { type: Number, required: true, default: 0 },
    lockUntil: { type: Number }
});

UserSchema.virtual('isLocked').get(function() {
    // check for a future lockUntil timestamp
    return !!(this.lockUntil && this.lockUntil > Date.now());
});

/*
!!!IMPORTANT!!!
 Mongoose middleware is not invoked on update() operations, so you must use a save() if you want to update user passwords.
 */

UserSchema.pre('save', function (next) {
   var user = this;
    console.log('user in pre save:');
    console.log(JSON.stringify(user));
    if (!user.isModified('password')) return next();

    bcrypt.genSalt(saltFactor, (err, salt) => {
        if (err) return next(err);

        bcrypt.hash(user.password, salt, null, (err, hash) => {
            if (err) return next(err);
            console.log('hash');
            user.password = hash;
            console.log('pw hash: '+hash);
            next();
        })
    })
});

UserSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
      if (err) return err;
      cb(null, isMatch);
  })
};

UserSchema.methods.incLoginAttempts = function(cb) {
    // if we have a previous lock that has expired, restart at 1
    if (this.lockUntil && this.lockUntil < Date.now()) {
        return this.update({
            $set: { loginAttempts: 1 },
            $unset: { lockUntil: 1 }
        }, cb);
    }
    // otherwise we're incrementing
    var updates = { $inc: { loginAttempts: 1 } };
    // lock the account if we've reached max attempts and it's not locked already
    if (this.loginAttempts + 1 >= maxLogins && !this.isLocked) {
        updates.$set = { lockUntil: Date.now() + lockOutTime };
    }
    return this.update(updates, cb);
};

const reasons = UserSchema.statics.failedLogin = {
    NOT_FOUND: 0,
    PASSWORD_INCORRECT: 1,
    MAX_ATTEMPTS: 2
};

UserSchema.statics.getAuthenticated = function(username, password, cb) {
    this.findOne({ username: username }, function(err, user) {
        console.log('getAuth called');
        if (err) return cb(err);

        // make sure the user exists
        if (!user) {
            console.log('user does not exist');
            return cb(null, null, reasons.NOT_FOUND);

        }

        // check if the account is currently locked
        if (user.isLocked) {
            console.log('user locked');
            // just increment login attempts if account is already locked
            return user.incLoginAttempts(function(err) {
                if (err) return cb(err);
                return cb(null, null, reasons.MAX_ATTEMPTS);
            });
        }

        // test for a matching password
        user.comparePassword(password, function(err, isMatch) {
            if (err) return cb(err);

            // check if the password was a match
            if (isMatch) {
                console.log('match pw');
                // if there's no lock or failed attempts, just return the user
                if (!user.loginAttempts && !user.lockUntil) return cb(null, user);
                // reset attempts and lock info
                var updates = {
                    $set: { loginAttempts: 0 },
                    $unset: { lockUntil: 1 }
                };
                return user.update(updates, function(err) {
                    if (err) return cb(err);
                    return cb(null, user);
                });
            }

            // password is incorrect, so increment login attempts before responding
            user.incLoginAttempts(function(err) {
                if (err) return cb(err);
                return cb(null, null, reasons.PASSWORD_INCORRECT);
            });
        });
    });
};


exports.UserModel = mongoose.model('user', UserSchema);
