import React from 'react';
import Navbar from './Navbar.jsx'
import {Row, Col} from 'react-bootstrap';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { appReducer } from '../reducers/appReducer';

console.log('Outer.jsx called');

const store = createStore(appReducer);

var Outer = React.createClass({
    render: function () {
        return (

            <html>
            <head>
                <title>{this.props.title}</title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css" />
                <link rel="stylesheet" href="app.css" />
            </head>
            <body>
            {/* Using the provider wrapper here and calling connect on the components that need it (check Navbar.jsx in my case) will pass the store down to the components that need it! 
             */}
            <Provider store={store}>
            <Row>
                <Navbar />
                <Col md={8} mdOffset={2} xs={10} xsOffset={1} className="modal-container">
                    {this.props.children}
                </Col>
            </Row>
            </Provider>
            {/*Remember, public folder is already set to static in server.js so no need to reference to public folder in src='' */}
            <script src='/bundle.js'></script>
            <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
            </body>
            </html>

        )
    }
});

module.exports = Outer;