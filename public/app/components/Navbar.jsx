import React from 'react';
import {Nav, Navbar, NavItem, Input, Button, Modal, OverlayTrigger, Popover, Tooltip, FormControl, FormGroup, ControlLabel, Well, Form} from 'react-bootstrap';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { logIn, logOut, signUp, showModal, closeModal, switchForm, showError} from '../actions/actions';
import jwtDecode from 'jwt-decode';

console.log('Navbar.jsx called');

let _NavbarComponent = React.createClass({
    componentDidMount() {
        this.props.CheckToken();
        console.log('state:');
        console.log(this.props.state);
        console.log('local storage:');
        console.log(window.localStorage);
    },
    submitLogIn() {
        let username = document.getElementById('loginUsername').value;
        let password = document.getElementById('loginPassword').value;
        let data = { 'username': username, 'password': password};
        $.ajax({
            url: '/authenticate',
            type: 'POST',
            data: data,
            success: (data) => {
                console.log('login successful, data: ');
                console.log(data);
                window.localStorage.setItem('user', data.user);
                window.localStorage.setItem('token', data.token);
                this.props.LogIn();
                setTimeout(this.props.Close, 1000);
            },
            error: (err) => {
                console.log('login failed: err');
                console.log(err);
                this.props.ShowError(err);
            }
        });
    },
    createUser() {
        let username = document.getElementById('signupUsername').value;
        let password = document.getElementById('signupPassword').value;
        let email = document.getElementById('signupEmail').value;
        let data = { username, password, email};
        $.ajax({
            url: '/signup',
            type: 'POST',
            data: data,
            success: (data) => {
                console.log('created user successful, data: ');
                console.log(data);
                this.props.signedUp();
                setTimeout(this.props.switchLogIn, 1000);
            },
            error: (err) => {
                console.log('login failed: err');
                console.log(err);
                this.props.ShowError(err);
            }
        });
    },
    openCreatePoll() {
        console.log('opencreatepoll');
        if (this.props.state.loggedIn) {
            this.props.switchCreatePoll();
            this.props.Open();
        }
        else {
            this.props.switchLogIn();
            this.props.Open();
        }
    },
    submitPoll() {

    },
    render: function () {
        let logInWell;
        let signUpWell;
        let logInLogOutButton;
        let modal;

        if (this.props.state.loggedIn) {
            logInWell = <Well bsStyle="success" >
                Succesfully logged in
            </Well>;
            logInLogOutButton = <Button
                onClick={this.props.LogOut}
            >
                Log out
            </Button>
        }
        else {
            logInWell = <Well bsStyle="info" onClick={this.props.switchSignUp}>
                Click here to sign up
            </Well>;
            logInLogOutButton = <Button
                onClick={this.props.Open}
            >
                Log in
            </Button>;
        }

        if (this.props.state.signedUp) {
            signUpWell = <Well bsStyle="success" >
                Succesfully signed up
            </Well>
        }
        else {
            signUpWell = <Well bsStyle="info" onClick={this.props.switchLogIn} >
                Click here to log in with existing account
            </Well>
        }

        if (this.props.state.error !== undefined) {
            logInWell = <Well bsStyle="danger" >
                Encountered an error
            </Well>;
            signUpWell = logInWell;
        }


        if (this.props.state.form === 'logIn') {
            modal =
                <Modal show={this.props.state.showModal} onHide={this.props.Close}>
                    <Modal.Header closeButton>
                        <Modal.Title>Login</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        { logInWell }
                        <form id="login-form" >
                            <FormGroup controlId="formControlsEmail">
                                <ControlLabel>Username</ControlLabel>
                                <FormControl type="text" id="loginUsername" name="username" />
                            </FormGroup>
                            <FormGroup controlId="formControlsPassword">
                                <ControlLabel>Password</ControlLabel>
                                <FormControl type="password" id="loginPassword" name="password"/>
                            </FormGroup>
                            <Button
                                id="login-submit"
                                onClick={this.submitLogIn}
                            >
                                Submit
                            </Button>
                            <Button
                                onClick={this.props.Close}
                            >
                                Close
                            </Button>

                        </form>
                    </Modal.Body>
                </Modal>
        }
        else if (this.props.state.form === 'signUp') {
            modal =
                <Modal show={this.props.state.showModal} onHide={this.props.Close}>
                    <Modal.Header closeButton>
                        <Modal.Title>Sign up</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        { signUpWell }
                        <form id="signup-form" >
                            <FormGroup controlId="formControlsEmail">
                                <ControlLabel>Username</ControlLabel>
                                <FormControl type="text" placeholder="Choose a username" id="signupUsername" name="username" />
                            </FormGroup>
                            <FormGroup controlId="formControlsEmail">
                                <ControlLabel>Email address</ControlLabel>
                                <FormControl type="email" placeholder="Enter email" id="signupEmail" name="email" />
                            </FormGroup>
                            <FormGroup controlId="formControlsPassword">
                                <ControlLabel>Password</ControlLabel>
                                <FormControl type="password" id="signupPassword" name="password"/>
                            </FormGroup>
                            <Button id="signup-submit"
                                onClick={this.createUser}
                                >
                                Submit
                            </Button>
                            <Button
                                onClick={this.props.Close}
                                >
                                Close
                            </Button>

                        </form>
                    </Modal.Body>
                </Modal>
        }
        else if (this.props.state.form === 'create') {
            modal =
                <Modal show={this.props.state.showModal} onHide={this.props.Close}>
                    <Modal.Header closeButton>
                        <Modal.Title>Create a poll</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form id="create-form" >

                            <Button id="create-submit"
                                onClick={this.submitPoll}
                                >
                                Create
                            </Button>
                            <Button
                                onClick={this.props.Close}
                            >
                                Close
                            </Button>

                        </form>
                    </Modal.Body>
                </Modal>
        }
        return (
            <Navbar>
                <Navbar.Header>
                    <Navbar.Brand>
                        <Link to="/home"><span id="home-link">Vote.co</span></Link>
                    </Navbar.Brand>
                    <Navbar.Toggle/>
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <Link to="/home">
                            Home
                        </Link>
                        <Link to="/all">
                            All polls
                        </Link>
                    </Nav>
                    <div className="pull-right">
                        { logInLogOutButton }
                        <Button
                            onClick={this.openCreatePoll}
                            >
                            Create new poll
                        </Button>
                        <Form inline action="/search" className="inline">
                            <FormControl type="text" placeholder="Search" ref="search-field" id="search-field"/>
                            <Button type="submit">Search</Button>
                        </Form>
                            { modal }
                    </div>
                </Navbar.Collapse>
            </Navbar>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};
const mapDispatchToProps = (dispatch) => {
    return {
        LogOut: () => {
            window.localStorage.removeItem('user');
            window.localStorage.removeItem('token');
            dispatch(logOut());
        },
        Open: () => {
            dispatch(showModal())
        },
        CheckToken: () => {
            if (window.localStorage.token !== undefined) {
                //check that the token is not expired
                //THIS IS NOT VERIFICATION!
                // Merely intended to not give a false sense of the user being logged in
                let now = Date.now();
                let expiryStamp;
                let decoded;
                decoded = jwtDecode(window.localStorage.token);
                expiryStamp = decoded.exp;
                console.log('Expiry: '+expiryStamp );
                if (now > (expiryStamp*1000)) {
                    console.log('token expired');
                    this.props.LogOut()
                }
                else if (now < (expiryStamp*1000)) dispatch(logIn())
            }
        },
        LogIn: () => {
            dispatch(logIn());
        },
        signedUp: () => {
            dispatch(signUp());
        },
        switchSignUp: () => {
            console.log('switch signup: ');
            dispatch(switchForm('signUp'));
        },
        switchLogIn: () => {
            console.log('switch login: ');
            dispatch(switchForm('logIn'));
        },
        switchCreatePoll: () => {
            console.log('switch create: ');
            dispatch(switchForm('create'));
        },
        Close: () => {
            dispatch(closeModal())
        },
        ShowError: (err) => {
            dispatch(showError(err))
        }
    }
};

let NavbarComponent = connect(mapStateToProps, mapDispatchToProps)(_NavbarComponent);

module.exports = NavbarComponent;